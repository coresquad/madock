package main

import (
	"fmt"
	"time"
)

func GetContainerAge(containerTs time.Time) string {
	now := time.Now().UTC()
	age := now.Sub(containerTs)
	return fmt.Sprintf("%dh%dm", int(age.Hours())%24, int(age.Minutes())%60)
}
