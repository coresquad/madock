package main

import (
	"context"
	"io"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/client"
)

type docker struct {
	client *client.Client
}

func NewDocker(ctx context.Context, opt ...client.Opt) (*docker, error) {
	opt = append(opt, client.FromEnv)
	opt = append(opt, client.WithAPIVersionNegotiation())
	cli, err := client.NewClientWithOpts(opt...)
	if err != nil {
		return nil, err
	}
	return &docker{
		client: cli,
	}, nil
}

// ContainerList...
func (d *docker) ContainerList(ctx context.Context, options container.ListOptions) ([]types.Container, error) {
	return d.client.ContainerList(ctx, options)
}

// ContainerInspect
func (d *docker) ContainerInspect(ctx context.Context, containerID string) (types.ContainerJSON, error) {
	return d.client.ContainerInspect(ctx, containerID)
}

// ContainerLogs
func (d *docker) ContainerLogs(ctx context.Context, container string, options container.LogsOptions) (io.ReadCloser, error) {
	return d.client.ContainerLogs(ctx, container, options)
}

// CopyFromContainer..
func (d *docker) CopyFromContainer(ctx context.Context, containerID, srcPath string) (io.ReadCloser, types.ContainerPathStat, error) {
	return d.client.CopyFromContainer(ctx, containerID, srcPath)
}

// Close...
func (d *docker) Close() error {
	return d.client.Close()
}
