package main

import (
	"archive/tar"
	"context"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"time"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/pkg/stdcopy"
	"github.com/olekukonko/tablewriter"
	"github.com/urfave/cli/v2"
)

type app struct {
	config        *configBuilder
	configManager *ConfigManager
	docker        *docker
	table         *tablewriter.Table
}

func NewAppManager(ctx context.Context) (*app, error) {
	// config builder
	config := NewConfigBuilder()

	// config manager
	configManager := NewConfigManager()
	if err := configManager.InitEnvironment(); err != nil {
		return nil, err
	}

	// docker instance
	docker, err := NewDocker(ctx)
	if err != nil {
		return nil, err
	}

	// table builder
	table := tablewriter.NewWriter(os.Stdout)
	return &app{
		config:        config,
		configManager: configManager,
		docker:        docker,
		table:         table,
	}, nil
}

// ActionSetConfigHost....
func (a *app) ActionSetConfigHost(ctx *cli.Context) error {
	host := ctx.String("host")
	port := ctx.Int("port")
	a.config.SetHost(host).SetPort(port)
	return a.config.Build()
}

// action list container
func (a *app) ActionListContainer(ctx *cli.Context) error {
	defer a.docker.Close()
	// docker instance
	containers, err := a.docker.ContainerList(ctx.Context, container.ListOptions{})
	if err != nil {
		return err
	}

	// table...
	rows := [][]string{}
	header := []string{"CONTAINER ID", "NAMES", "CREATED", "AGE", "STATUS"}
	names := map[string]string{}

	// iterate,,,
	for _, v := range containers {

		// iterate name
		for _, name := range v.Names {
			name = strings.Replace(name, "/", "", -1)
			names[v.ID] = name
		}

		// dump name
		container, ok := names[v.ID]
		if !ok {
			rows = append(rows, []string{})
		}

		// createdAt
		createdAt := time.Unix(v.Created, 0).UTC()
		age := GetContainerAge(createdAt)

		// append data
		rows = append(rows,
			[]string{
				v.ID[:10],
				container,
				createdAt.Format(time.RFC3339),
				age,
				v.Status,
			},
		)
	}
	a.table.SetHeader(header)
	a.table.SetAutoWrapText(false)
	a.table.SetAutoFormatHeaders(true)
	a.table.SetHeaderAlignment(tablewriter.ALIGN_LEFT)
	a.table.SetAlignment(tablewriter.ALIGN_LEFT)
	a.table.SetCenterSeparator("")
	a.table.SetColumnSeparator("")
	a.table.SetRowSeparator("")
	a.table.SetHeaderLine(false)
	a.table.SetBorder(false)
	a.table.SetTablePadding("\t") // pad with tabs
	a.table.SetNoWhiteSpace(true)
	a.table.AppendBulk(rows) // Add Bulk Data
	a.table.Render()

	return nil
}

// ContainerInspect
func (a *app) ActionContainerInspect(ctx *cli.Context) error {
	var (
		rows   [][]string
		header []string
	)

	defer a.docker.Close()

	// table layout
	a.table.SetAutoWrapText(true)
	a.table.SetAutoFormatHeaders(true)
	a.table.SetHeaderAlignment(tablewriter.ALIGN_LEFT)
	a.table.SetAlignment(tablewriter.ALIGN_LEFT)

	// inspect by container id
	containerId := ctx.String("id")
	resp, err := a.docker.ContainerInspect(ctx.Context, containerId)
	if err != nil {
		return err
	}

	// inspect container env...
	if ctx.Bool("env") {
		rows, header = a.inspectEnv(resp)
		a.table.SetHeader(header)
		a.table.AppendBulk(rows) // Add Bulk Data
		a.table.SetCaption(true, fmt.Sprintf("Created At: %s", resp.Created))
		a.table.Render()
		return nil
	}

	// inspect container network
	if ctx.Bool("network") {
		rows, header = a.inspectNetwork(resp)
		a.table.SetHeader(header)
		a.table.AppendBulk(rows)
		a.table.Render()
		return nil
	}

	// inspect default
	rows, header = a.defaultInspect(resp)
	a.table.SetHeader(header)
	a.table.AppendBulk(rows) // Add Bulk Data
	a.table.SetCaption(true, fmt.Sprintf("Path: %s - Network: %s", resp.Path, resp.HostConfig.NetworkMode))
	a.table.Render()

	return nil
}

// inspectEnv...
func (a *app) inspectEnv(resp types.ContainerJSON) ([][]string, []string) {
	var (
		rows   [][]string
		header []string
	)

	header = append(header, "KEY", "VALUE")

	// iterate env variable...
	for _, b := range resp.Config.Env {
		idx := strings.Index(b, "=")
		if idx != -1 {
			key := b[:idx]
			val := b[idx+1:]
			rows = append(rows, []string{key, val})
		}
	}

	// sort to desc....
	sort.Slice(rows, func(i, j int) bool {
		return rows[i][0] < rows[j][0]
	})

	return rows, header
}

// inspectNetwork....
func (a *app) inspectNetwork(resp types.ContainerJSON) ([][]string, []string) {
	var (
		rows   [][]string
		header []string
		port   []string
	)

	// header
	header = append(header, "PORT", "NETWORK", "MAC ADDRESS", "IP ADDRESS", "GATEWAY")

	// iterate port...
	for k := range resp.NetworkSettings.Ports {
		protocol := fmt.Sprintf("%s/%s", k.Port(), strings.ToUpper(k.Proto()))
		port = append(port, protocol)
	}

	// network detail
	networkName := resp.HostConfig.NetworkMode.NetworkName()
	network, ok := resp.NetworkSettings.Networks[networkName]
	if !ok {
		rows = append(rows, []string{})
	}

	// row
	column := []string{
		strings.Join(port, ","),
		networkName,
		strings.ToUpper(network.MacAddress),
		fmt.Sprintf("%s/%d", network.IPAddress, network.IPPrefixLen),
		network.Gateway,
	}
	rows = append(rows, column)
	return rows, header
}

// defaultInspect...
func (a *app) defaultInspect(resp types.ContainerJSON) ([][]string, []string) {
	var (
		rows   [][]string
		header []string
	)

	// header..
	header = append(header, "ID", "NAME", "HEALTH STATUS", "WORKING DIR", "CONFIG")

	// rows
	configPath := filepath.Base(resp.Config.Labels["com.docker.compose.project.config_files"])
	column := []string{
		resp.ID[:10],
		strings.Replace(resp.Name, "/", "", -1),
		resp.State.Health.Status,
		resp.Config.WorkingDir,
		configPath,
	}
	rows = append(rows, column)

	return rows, header
}

// log
func (a *app) ActionContainerLog(ctx *cli.Context) error {
	defer a.docker.Close()
	// name
	containerName := ctx.String("name")
	flagFollow := ctx.Bool("f")
	flagTs := ctx.String("ts")

	// log option
	logOpt := container.LogsOptions{
		ShowStdout: true,
		ShowStderr: true,
	}

	// if flag follow is true
	if flagFollow {
		logOpt.Follow = true
	} else {
		logOpt.Follow = false
	}

	// timestamp
	if flagTs != "" {
		logOpt.Since = flagTs
	}

	logReader, err := a.docker.ContainerLogs(ctx.Context, containerName, logOpt)
	if err != nil {
		return err
	}
	defer logReader.Close()

	if _, err := stdcopy.StdCopy(os.Stdout, os.Stderr, logReader); err != nil {
		return err
	}

	return nil
}

// ActionContainerDownload....
func (a *app) ActionContainerDownload(clctx *cli.Context) error {

	var (
		ctx     = clctx.Context
		flagId  = clctx.String("id")
		flagSrc = clctx.String("src")
		flagDst = clctx.String("dst")
	)

	ctx, cancel := context.WithTimeout(ctx, 10*time.Second)
	defer cancel()

	// reader
	reader, _, err := a.docker.CopyFromContainer(ctx, flagId, flagSrc)
	if err != nil {
		return err
	}
	if err != nil {
		return err
	}
	defer reader.Close()

	td := tar.NewReader(reader)
	if _, err := td.Next(); err != nil {
		return err
	}

	f, _ := os.Create(flagDst)
	if _, err := io.Copy(f, td); err != nil {
		return err
	}

	return nil
}
