package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"time"

	"gopkg.in/yaml.v3"
)

type configBuilder struct {
	connection ConnectionConfig
}

func NewConfigBuilder() *configBuilder {
	return &configBuilder{
		connection: ConnectionConfig{},
	}
}

func (cb *configBuilder) SetHost(val string) *configBuilder {
	if val == "" {
		val = "127.0.0.1"
	}
	cb.connection.Host = val
	return cb
}

func (cb *configBuilder) SetPort(val int) *configBuilder {
	if val == 0 {
		val = 2375
	}
	cb.connection.Port = val
	return cb
}

func (cb *configBuilder) Build() error {
	url := fmt.Sprintf("tcp://%s:%d", cb.connection.Host, cb.connection.Port)
	connectionConfig := ConnectionConfig{
		Host: cb.connection.Host,
		Port: cb.connection.Port,
		URL:  url,
	}
	configBody := Config{
		CreatedAt:        time.Now().UTC(),
		ConnectionConfig: connectionConfig,
	}
	buf, err := yaml.Marshal(configBody)
	if err != nil {
		return err
	}
	if err := ioutil.WriteFile("config.yaml", buf, 0777); err != nil {
		return err
	}
	return nil
}

// config manager
// handle read config
type ConfigManager struct {
}

func NewConfigManager() *ConfigManager {
	return &ConfigManager{}
}

func (cb *ConfigManager) GetConfig() (Config, error) {
	var payload Config
	body, err := ioutil.ReadFile("config.yaml")
	if err != nil {
		return payload, err
	}
	if err := yaml.Unmarshal(body, &payload); err != nil {
		return payload, err
	}
	return payload, nil
}

func (cb *ConfigManager) InitEnvironment() error {
	config, err := cb.GetConfig()
	if err != nil {
		return err
	}
	os.Setenv("DOCKER_HOST", config.ConnectionConfig.URL)
	return nil
}
