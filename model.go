package main

import "time"

type ConnectionConfig struct {
	Host string `yaml:"host"`
	Port int    `yaml:"port"`
	URL  string `yaml:"url"`
}
type Config struct {
	ConnectionConfig ConnectionConfig `yaml:"connection"`
	CreatedAt        time.Time        `yaml:"created_at"`
}
