// main.go
package main

import (
	"context"
	"log"
	"os"

	cli "github.com/urfave/cli/v2"
)

func main() {
	ctx := context.Background()
	appm, err := NewAppManager(ctx)
	if err != nil {
		log.Println(err)
		return
	}
	// app
	app := &cli.App{
		Commands: []*cli.Command{
			{
				Name:  "config",
				Usage: "prepare config file",
				Subcommands: []*cli.Command{
					{
						Name:   "connection",
						Action: appm.ActionSetConfigHost,
						Flags: []cli.Flag{
							&cli.StringFlag{
								Name:     "host",
								Value:    "",
								Required: true,
							},
							&cli.IntFlag{
								Name:     "port",
								Value:    0,
								Required: true,
							},
						},
					},
				},
			},
			{
				Name: "container",
				Subcommands: []*cli.Command{
					{
						Name:   "ls",
						Action: appm.ActionListContainer,
					},
					{
						Name: "log",
						Flags: []cli.Flag{
							&cli.StringFlag{
								Name:     "name",
								Value:    "",
								Required: true,
							},
							&cli.BoolFlag{
								Name:  "f",
								Value: false,
							},
							&cli.StringFlag{
								Name:  "ts",
								Value: "",
							},
						},
						Action: appm.ActionContainerLog,
					},
					{
						Name: "inspect",
						Flags: []cli.Flag{
							&cli.StringFlag{
								Name:  "id",
								Value: "",
							},
							&cli.BoolFlag{
								Name: "env",
							},
							&cli.BoolFlag{
								Name:  "network",
								Value: false,
							},
						},
						Action: appm.ActionContainerInspect,
					},
					{
						Name: "download",
						Flags: []cli.Flag{
							&cli.StringFlag{
								Name: "id",
							},
							&cli.StringFlag{
								Name: "src",
							},
							&cli.StringFlag{
								Name: "dst",
							},
						},
						Action: appm.ActionContainerDownload,
					},
				},
			},
		},
	}
	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
		return
	}
}
